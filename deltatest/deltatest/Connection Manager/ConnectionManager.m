//
//  ConnectionManager.m
//  deltatest
//
//  Created by Raúl Blánquez on 14/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "ConnectionManager.h"
#import "Utils.h"

@implementation ConnectionManager

+ (void)sendCoordinates:(CLLocationCoordinate2D)location withShipId:(NSString *)shipId withShipName:(NSString *)shipName andTimeStamp:(NSDate *)timestamp
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString *name = [shipName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *requestString = [NSString stringWithFormat:@"%@/setData%@.php?UUID=%@&name=%@&lat=%f&lon=%f&timestamp=%f",BASE_URL, [Utils getVersion], shipId, name, location.latitude, location.longitude, [timestamp timeIntervalSince1970]];
    
    [manager POST:requestString
      parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
          
          //Response Data
          NSMutableDictionary *responseDic = (NSMutableDictionary *)responseObject;
          
          NSMutableDictionary *results = [NSMutableDictionary dictionaryWithDictionary:responseDic];
          [results setObject:[NSNumber numberWithBool:YES]
                      forKey:@"success"];
          
          [[NSNotificationCenter defaultCenter] postNotificationName:kPostNotifCoordinatesSent
                                                              object:nil
                                                            userInfo:results];
          
      } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          
          NSString *errMsg = [operation.responseObject objectForKey:@"response"];
          
          NSDictionary *resultsDic = @{@"success":[NSNumber numberWithBool:NO], @"error": errMsg ? errMsg : NSLocalizedString(@"dialog_error_network", Nil)};
          
          [[NSNotificationCenter defaultCenter] postNotificationName:kPostNotifCoordinatesSent
                                                              object:nil
                                                            userInfo:resultsDic];
      }];
}

@end
