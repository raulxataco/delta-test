//
//  ConnectionManager.h
//  deltatest
//
//  Created by Raúl Blánquez on 14/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import <CoreLocation/CoreLocation.h>

#define kPostNotifCoordinatesSent     @"coordinatesSent"

@interface ConnectionManager : NSObject

+ (void)sendCoordinates:(CLLocationCoordinate2D)location
             withShipId:(NSString *)shipId
           withShipName:(NSString *)shipName
           andTimeStamp:(NSDate *)timestamp;

@end
