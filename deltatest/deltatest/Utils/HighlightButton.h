//
//  HighlightButton.h
//  DigitalHealth
//
//  Created by mobdev1 on 30/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightButton : UIButton

@property (nonatomic, strong) UIColor *mainColor;

@end
