//
//  Utils.m
//  Presencial
//
//  Created by Raúl Blánquez on 11/05/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString *)getVersion
{
    return ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]);
}

+ (NSString *)deviceID
{
    NSUUID *deviceID = [[UIDevice currentDevice] identifierForVendor];
    return deviceID.UUIDString;
}

@end
