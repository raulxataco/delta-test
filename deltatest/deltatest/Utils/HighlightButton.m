//
//  HighlightButton.m
//  DigitalHealth
//
//  Created by mobdev1 on 30/05/14.
//  Copyright (c) 2014 Axa. All rights reserved.
//

#import "HighlightButton.h"

@implementation HighlightButton

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.mainColor = self.backgroundColor;
    }
    return self;
}

- (void)awakeFromNib
{
    self.mainColor = self.backgroundColor;
}

-(void) setHighlighted:(BOOL)highlighted
{
    if (highlighted)
    {
        CGFloat red, green, blue, alpha;
        [self.mainColor getRed:&red green:&green blue:&blue alpha:&alpha];

        if (alpha == 0)
        {
            self.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.2];
        }
        else
        {
            self.backgroundColor = [self darkerColor:self.mainColor];
        }
    }
    else
    {
        self.backgroundColor = self.mainColor;
    }
    [super setHighlighted:highlighted];
}

- (UIColor *)darkerColor:(UIColor *)currentColor
{
    CGFloat red, green, blue, alpha;
    [currentColor getRed:&red green:&green blue:&blue alpha:&alpha];
    CGFloat multiplier = 0.8f;
    return [UIColor colorWithRed:red*multiplier green:green*multiplier blue:blue*multiplier alpha:alpha];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
