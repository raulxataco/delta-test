//
//  Utils.h
//  Presencial
//
//  Created by Raúl Blánquez on 11/05/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString *)getVersion;
+ (NSString *)deviceID;

@end
