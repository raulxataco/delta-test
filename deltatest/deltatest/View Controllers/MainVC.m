//
//  MainVC.m
//  deltatest
//
//  Created by Raúl Blánquez on 13/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#define kStartDate      @"01-08-2014 "
#define kStopDate       @"31-08-2014"
#define kTagAlertText   1

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#import "MainVC.h"
#import "MapaVC.h"

@interface MainVC ()
{
    NSDateFormatter     *_dateFormatter;
    NSDate              *_startDate, *_stopDate;
    UITextField         *_shipName;
    
}
@property (nonatomic, weak) MapaVC *mapaVC;

@end

@implementation MainVC
// *****************************************************************************
#pragma mark -                                          ViewController Lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self localizeLabels];
    [self initializeDate];
    [self makeView];
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSNumber *value = [def objectForKey:@"firstLaunch"];
    if (!value)
    {
        [self doHelp:self];
        [def setObject:[NSNumber numberWithBool:NO] forKey:@"firstLaunch"];
        [def synchronize];
    }    
}

- (void)viewWillAppear:(BOOL)animated
{
    //[self.txtShipName becomeFirstResponder];
    self.navigationController.navigationBar.hidden = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    self.mapaVC = segue.destinationViewController;
    
    self.mapaVC.title = self.valShipName.text;
}

// *****************************************************************************
#pragma mark -                                          AlertView Delegate
// *****************************************************************************
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //[self.txtShipName becomeFirstResponder];
    if (alertView.tag == kTagAlertText && buttonIndex == 0)
    {
        _shipName = [alertView textFieldAtIndex:0];
        
        if (_shipName.text && [_shipName.text length] > 0)
        {
            self.valShipName.text = [_shipName.text stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                      withString:[[_shipName.text substringToIndex:1] capitalizedString]];
        }
    }
}

// *****************************************************************************
#pragma mark -                                          Target Actions
// *****************************************************************************
- (IBAction)requestShipName:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil
                                                    message:NSLocalizedString(@"main_shipname_placeholder", Nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                          otherButtonTitles:NSLocalizedString(@"cancel", Nil), nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = kTagAlertText;
    UITextField *textfield = [alert textFieldAtIndex:0];
    textfield.text = _shipName.text;
    [alert show];
}

- (IBAction)doOK:(id)sender
{
    if (_shipName.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", Nil)
                                                        message:NSLocalizedString(@"main_error_shipname", Nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                              otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        if ([self testIsAvailable])
        {
            [self performSegueWithIdentifier:@"mapa" sender:self];
        }
    }
}

- (IBAction)doHelp:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"main_btn_help", Nil)
                                                    message:NSLocalizedString(@"help_text", Nil)
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"help_btn_ok", Nil)
                                          otherButtonTitles: nil];
    [alert show];
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)localizeLabels
{
    self.lblShipName.text = NSLocalizedString(@"main_shipname_label", Nil);
    self.valShipName.text = NSLocalizedString(@"main_shipname_placeholder", Nil);
    [self.btnOK setTitle:NSLocalizedString(@"main_btn_ok", Nil) forState:UIControlStateNormal];
    [self.btnHelp setTitle:NSLocalizedString(@"main_btn_help", Nil) forState:UIControlStateNormal];
}

- (BOOL)testIsAvailable
{
    if ([self.valShipName.text isEqualToString:kTestShipName]) return YES;
    /*
    //Check if we're in date
    NSDate *today = [NSDate date];
    
    if ([today compare:_startDate] == NSOrderedAscending)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"main_error_earlierdate", Nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                              otherButtonTitles: nil];
        [alert show];

        return NO;
    }
    
    if ([today compare:_stopDate] == NSOrderedDescending)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"main_error_laterdate", Nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                              otherButtonTitles: nil];
        [alert show];

        return NO;
    }*/
    
    return YES;
}

- (void)initializeDate
{
    _dateFormatter = [[NSDateFormatter alloc] init];
    [_dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSString *startDateString = kStartDate;
    _startDate = [_dateFormatter dateFromString:startDateString];
    
    NSString *stopDateString = kStopDate;
    _stopDate = [_dateFormatter dateFromString:stopDateString];
}

- (void)makeView
{
    self.valShipName.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(12));
    
    //[self.txtShipName editingRectForBounds:self.txtShipName.bounds];
}

@end
