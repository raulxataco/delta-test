//
//  MapaVC.m
//  deltatest
//
//  Created by Raúl Blánquez on 14/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import "MapaVC.h"
#import "ConnectionManager.h"
#import "Reachability.h"
#import "SVProgressHUD.h"
#import "Utils.h"

#define kRefLatitude    40.7177777777 //"40-43-04N"
#define kRefLongitude   1.35944444444 //"1-21-34E"

#define kMaxRegion      50            //Km
#define kMaxDistance    200           //m

@interface MapaVC ()
{
    Reachability *_internetReachability;
    CLLocationCoordinate2D _userCoords, _stationCoords;
    CLLocation *_userLocation, *_stationLocation;
}
@property (nonatomic, strong) CLLocationManager  *locationManager;

@end

@implementation MapaVC

// *****************************************************************************
#pragma mark -                                          ViewController Lifecycle
// *****************************************************************************
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self localizeLabels];
    [self initLocationManager];
    
    self.navigationController.navigationBar.hidden = NO;

    self.recordingEnabled = NO;    
}

- (void)dealloc
{
    DLog();
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.recordingEnabled = NO;
    [self.locationManager stopUpdatingLocation];
}

// *****************************************************************************
#pragma mark -                                         CoreLocationManager
// *****************************************************************************
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    DLog();
    
    CLLocationCoordinate2D centerCoord = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
    
    _userCoords = centerCoord;
    
    [self updateMapViewTo:_userCoords withZoom:1000];
    
    int latdegrees = fabs(_userCoords.latitude);
    float latfminutes = (fabs(_userCoords.latitude) - latdegrees) * 60;
    
    int latminutes = latfminutes;
    float latfseconds = (latfminutes - latminutes) * 60;
    
    int londegrees = fabs(_userCoords.longitude);
    float lonfminutes = (fabs(_userCoords.longitude) - londegrees) * 60;
    
    int lonminutes = lonfminutes;
    float lonfseconds = (lonfminutes - lonminutes) * 60;
    
    NSString *latsign = _userCoords.latitude < 0 ? @"S" : @"N";
    NSString *lonsign = _userCoords.longitude < 0 ? @"W" : @"E";
    
    self.lblActualPosition.text = [NSString stringWithFormat:@"%dº %d' %.2f''%@, %dº %d' %.2f''%@", latdegrees, latminutes, latfseconds, latsign, londegrees, lonminutes, lonfseconds, lonsign];
}

// *****************************************************************************
#pragma mark -                                          Target actions
// *****************************************************************************
- (IBAction)doSendPosition:(id)sender
{
    //Check if we're inside the test region
    _userLocation = [[CLLocation alloc] initWithLatitude:_userCoords.latitude
                                               longitude:_userCoords.longitude];
    
    CLLocationDistance distance = [_userLocation distanceFromLocation:_stationLocation]/1000;
    
    DLog(@"Distance = %f Km", distance);
    
    if (distance > kMaxRegion && ![self.title isEqualToString:kTestShipName])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"main_error_notinregion", Nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                              otherButtonTitles: nil];
        [alert show];
    }
    else
    {
        [self sendData];       
    }
}

- (IBAction)doStartRegister:(id)sender
{
    self.recordingEnabled = !self.recordingEnabled;
    
    [self.btnRegister setTitle:self.recordingEnabled ? NSLocalizedString(@"map_btn_stop", nil) : NSLocalizedString(@"map_btn_start", Nil)
                          forState:UIControlStateNormal];
    
    self.btnSendPosition.enabled = !self.recordingEnabled;
}

// *****************************************************************************
#pragma mark -                                   ConnectionManager Notifications
// *****************************************************************************
- (void)coordinatesSent:(NSNotification *)notif
{
    [SVProgressHUD dismiss];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:kPostNotifCoordinatesSent
                                                  object:nil];
        
    DLog(@"%@", [notif userInfo]);
}

// *****************************************************************************
#pragma mark -                                          Public methods
// *****************************************************************************
- (void)updateMapViewTo:(CLLocationCoordinate2D)location withZoom:(CGFloat)meters
{
    DLog(@"%f %f", location.latitude, location.longitude);
    
    _currentLocation = location;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, meters, meters);

    [self.mapa setRegion:viewRegion animated:YES];
    
    if (self.recordingEnabled)
    {
        //Check if we're inside the test region
        _userLocation = [[CLLocation alloc] initWithLatitude:location.latitude
                                                   longitude:location.longitude];
        
        CLLocationDistance distance = [_userLocation distanceFromLocation:_stationLocation]/1000;
        
        DLog(@"Distance = %f Km", distance);
        
        if (distance <= kMaxRegion || [self.title isEqualToString:kTestShipName])
        {
            [self sendData];
        }
    }
}

// *****************************************************************************
#pragma mark -                                          Private methods
// *****************************************************************************
- (void)localizeLabels
{
    [self.btnSendPosition setTitle:NSLocalizedString(@"map_btn_send", nil) forState:UIControlStateNormal];
    [self.btnRegister setTitle:NSLocalizedString(@"map_btn_start", Nil) forState:UIControlStateNormal];
}

- (void)initLocationManager
{
    if (![CLLocationManager locationServicesEnabled])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:NSLocalizedString(@"main_error_gpsnotavailable", Nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"ok", Nil)
                                              otherButtonTitles: nil];
        [alert show];
        
        return;
    }

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kMaxDistance;
    [self.locationManager startUpdatingLocation];
    
    // CLLocation de la estación
    _stationLocation = [[CLLocation alloc] initWithLatitude:kRefLatitude
                                                  longitude:kRefLongitude];
    
    self.lblActualPosition.text = @"";
}

- (void)sendData
{
    _internetReachability = [Reachability reachabilityForInternetConnection];
	[_internetReachability startNotifier];
    NetworkStatus netStatus = [_internetReachability currentReachabilityStatus];
    
    if (netStatus != NotReachable)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(coordinatesSent:)
                                                     name:kPostNotifCoordinatesSent
                                                   object:nil];
        
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeClear];
        
        [ConnectionManager sendCoordinates:_currentLocation
                                withShipId:[Utils deviceID]
                              withShipName:self.title
                              andTimeStamp:[NSDate date]];
    }
    else
    {
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"", Nil)];
    }
}

@end
