//
//  MainVC.h
//  deltatest
//
//  Created by Raúl Blánquez on 13/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainVC : UIViewController <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblShipName;
@property (weak, nonatomic) IBOutlet UIButton *btnOK;
@property (weak, nonatomic) IBOutlet UIButton *btnHelp;
@property (weak, nonatomic) IBOutlet UILabel *valShipName;

- (IBAction)requestShipName:(id)sender;
- (IBAction)doOK:(id)sender;
- (IBAction)doHelp:(id)sender;
@end
