//
//  MapaVC.h
//  deltatest
//
//  Created by Raúl Blánquez on 14/07/14.
//  Copyright (c) 2014 Xataco. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;
@import CoreLocation;

@interface MapaVC : UIViewController <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapa;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnSendPosition;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;
@property (nonatomic, assign) BOOL recordingEnabled;
@property (weak, nonatomic) IBOutlet UILabel *lblActualPosition;

- (void)updateMapViewTo:(CLLocationCoordinate2D)location withZoom:(CGFloat)meters;
- (IBAction)doStartRegister:(id)sender;
- (IBAction)doSendPosition:(id)sender;

@end
